#!/bin/bash

set -e

DEPS="yq curl"
ENV_VARS="CONFIG_FILE"

check_env_vars () {
	for var in ${ENV_VARS}
	do
		if [ -z "${!var}" ]
		then
			echo "Env var missing: $var"
			exit 1
		fi
	done
}

check_deps () {
	for dep in ${DEPS}
	do
		local ret=$(command -v ${dep})
		if [ "${ret}" == "" ]
		then
			echo "Dependency missing: ${dep}"
			exit 2
		fi
	done
}


install_custom_cards () {
	echo "### Processing custom cards ###"
	echo
	custom_card_folder=$(yq .custom_card_folder "${CONFIG_FILE}")
	readarray custom_cards < <(yq e -o=j -I=0 '.["cards"][]' "${CONFIG_FILE}")
	for custom_card in ${custom_cards[@]}
	do
		url=$(echo ${custom_card} | yq e .url)
		echo Processing: $url
		name=$(echo ${custom_card} | yq e .name)
		if [ "${url}" == "null" ]
		then
			echo "Missing url attribute for a custom card"
			exit 3
		fi
		if [[ "${url}" == *.tar.gz ]]
		then
			if [ "${name}" == "null" ]
			then
				echo "Missing name attribute for a custom card"
				exit 4
			fi
			rm -vfr "${custom_card_folder}/${name}"
			mkdir -p "${custom_card_folder}/${name}"
			curl --fail -L "${url}" -o "${custom_card_folder}/${name}/archive.tar.gz"
			tar xvf "${custom_card_folder}/${name}/archive.tar.gz" --strip-components 2 -C ${custom_card_folder}/${name}
			rm -vf ${custom_card_folder}/${name}/archive.tar.gz
		else
			if [[ "${url}" == *.js ]]
			then
				if [ "${name}" == "null" ]
				then
					filename=$(basename ${url})
					name="${filename%.*}"
				fi
				rm -vf "${custom_card_folder}/${name}.js"
				curl --fail -L "${url}" -o "${custom_card_folder}/${name}.js"
			fi
		fi
		echo Custom card $name installed
		echo
	done
}


install_custom_integrations () {
	echo "### Processing custom integration###"
	echo
	custom_integration_folder=$(yq .custom_integration_folder "${CONFIG_FILE}")
	readarray custom_integrations < <(yq e -o=j -I=0 '.["integrations"][]' ${CONFIG_FILE})
	for custom_integration in ${custom_integrations[@]}
	do
		url=$(echo ${custom_integration} | yq e .url)
		echo Processing: $url
		name=$(echo ${custom_integration} | yq e .name)
		if [ "${url}" == "null" ]
		then
			echo "Missing url attribute for a custom integration"
			exit 4
		fi
		filename=$(basename ${url})
		if [ "${name}" == "null" ]
		then
			name="${filename%.*}"
		fi	
		rm -rf /tmp/${name}
		mkdir -p /tmp/${name}
		curl --silent --fail -L "${url}" -o /tmp/${name}/${filename}
		tar xzf /tmp/${name}/${filename} -C /tmp/${name}
		custom_components=$(ls /tmp/${name}/*/custom_components/)
		for custom_component in ${custom_components}
		do
			echo Installing custom component: $custom_component
			rm -rvf ${custom_integration_folder}/${custom_component} || true
			mkdir -p ${custom_integration_folder}
			cp -vr /tmp/${name}/*/custom_components/${custom_component} ${custom_integration_folder}/${custom_component}
		done
		echo Custom integration $name installed
		echo
	done
}





check_env_vars
check_deps
install_custom_cards
install_custom_integrations
