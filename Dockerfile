FROM debian:sid

RUN apt update && apt install -y curl

RUN curl --fail -L https://github.com/mikefarah/yq/releases/download/v4.35.2/yq_linux_amd64 -o /usr/local/bin/yq
RUN chmod +x /usr/local/bin/yq

COPY entrypoint.sh /entrypoint.sh

CMD /entrypoint.sh
